package org.mayun.utils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
/**
 * 数据库连接
 * @author hasee
 *
 */
public class SqlHelper {
	private static Properties properties=new Properties();
	public static final String DRIVER="driver";
	public static final String URL="url";
	public static final String USER="user";
	public static final String PASSWORD="password";
	static ThreadLocal<Connection> local=new ThreadLocal<Connection>();
	/**
	 * 获取驱动
	 * @param
	 */
	static{
		try {
			properties.load(SqlHelper.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	static{
		try {
			Class.forName(properties.getProperty(DRIVER));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 *链接数据库
	 */
	public static Connection openConnection(){
		Connection conn=local.get();
		try {
			if(conn==null||conn.isClosed()){
				conn=DriverManager.getConnection(properties.getProperty(URL),properties.getProperty(USER),properties.getProperty(PASSWORD));
				local.set(conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	/**
	 *查询
	 * @param <T>
	 */
	public static List<HashMap<String,Object>> select(String sql,Object ... parmes){
		ResultSet rs=null;
		PreparedStatement pst=null;
		List<HashMap<String,Object>> rows=new ArrayList<HashMap<String,Object>>();
		try {
			Connection conn=SqlHelper.openConnection();
			pst=conn.prepareStatement(sql);
			if(parmes!=null){
				for(int i=0;i<parmes.length;i++){
					pst.setObject(i+1,parmes[i]);
				}
			}
			rs=pst.executeQuery();
			ResultSetMetaData res=rs.getMetaData();
			int l=res.getColumnCount();
			while(rs.next()){
				HashMap<String,Object> maps=new HashMap<String,Object>();
				for(int i=0;i<l;i++){
					String label=res.getColumnLabel(i+1);
					maps.put(label,rs.getObject(label));
				}
				rows.add(maps);
			}
		} catch (SQLException e) {
			throw new RuntimeException("查询失败",e);
		} finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return rows;
	}
	/**
	 *修改
	 */
	public static int update(String sql,Object ... parmes){
		int row=0;
		PreparedStatement pst=null;
		Connection conn=SqlHelper.openConnection();
		try {
			pst=conn.prepareStatement(sql);
			if(parmes.length!=0){
				for(int i=0;i<parmes.length;i++){
					pst.setObject(i+1,parmes[i]);
				}
			}
			row=pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException("修改失败",e);
		} finally{
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return row;
	}
	/**
	 * 关闭
	 */
	public static void closeConnection(){
		Connection conn=local.get();
		if(conn!=null){
			try {
				conn.close();
				conn=null;
				local.remove();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public static interface RowHandlerMapper<T>{
		public  List<T> maping(ResultSet rs);
	}
}